package fr.wedoogift.common.exceptions;

public class InvalidAmount extends Exception{

    public InvalidAmount(String message) {
        super(message);
    }
}
