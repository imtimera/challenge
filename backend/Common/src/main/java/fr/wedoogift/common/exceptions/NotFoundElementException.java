package fr.wedoogift.common.exceptions;


public class NotFoundElementException extends Exception {

    public NotFoundElementException(String message) {
        super(message);
    }
}
