package fr.wedoogift.common.models;

import java.io.Serializable;

/* use
@Getter
@Setter
@Constructor
...(if necessary)
by adding lombok
 */
public abstract class CommonUser<T> implements Serializable {
    protected Long id;

    protected T balance;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public T getBalance() {
        return balance;
    }

    public void setBalance(T balance) {
        this.balance = balance;
    }

    public abstract void receiveDistribution(T amount);


}
