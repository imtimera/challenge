package fr.wedoogift.common.models;

import java.io.Serializable;

/* use
@Getter
@Setter
@Constructor
...(if necessary)
by adding lombok
 */
public class Company implements Serializable {
    private Long id;

    private String name;

    private double balance;

    public Company() {
        super();
    }

    public Company(Long id, String name, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void giveDistribution(double amount){
        this.balance -= amount;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
