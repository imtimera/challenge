package fr.wedoogift.common.models;

import java.io.Serializable;

/* use
@Getter
@Setter
@Constructor
...(if necessary)
by adding lombok
 */
public class Distribution implements Serializable {
    protected Long id;

    protected String start_date;

    protected String end_date;

    protected Long company_id;

    protected Long user_id;

    protected double amount;

    public Distribution() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Distribution(Long id, double amount, String start_date, String end_date, Long company_id, Long user_id) {
        this.id = id;
        this.amount = amount;
        this.start_date = start_date;
        this.end_date = end_date;
        this.company_id = company_id;
        this.user_id = user_id;
    }

}
