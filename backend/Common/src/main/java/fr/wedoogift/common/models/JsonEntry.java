package fr.wedoogift.common.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* use
@Getter
@Setter
@Constructor
...(if necessary)
by adding lombok
 */
public class JsonEntry<T extends CommonUser, S extends Distribution> implements Serializable {
    protected List<T> users;

    protected List<Company> companies;

    protected List<S> distributions;

    public JsonEntry(List<T> users, List<Company> companies, List<S> distributions) {
        this.users = users;
        this.companies = companies;
        this.distributions = distributions;
    }

    public JsonEntry() {
        super();
        this.users = new ArrayList<>();
        this.companies = new ArrayList<>();
        this.distributions = new ArrayList<>();
    }

    public List<T> getUsers() {
        return users;
    }

    public void setUsers(List<T> users) {
        this.users = users;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<? extends Distribution> getDistributions() {
        return distributions;
    }

    public void setDistributions(List<S> distributions) {
        this.distributions = distributions;
    }

    public void addDistribution(S distribution) {
        this.distributions.add(distribution);
    }
}
