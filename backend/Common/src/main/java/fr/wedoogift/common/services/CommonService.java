package fr.wedoogift.common.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.models.CommonUser;
import fr.wedoogift.common.models.Company;
import fr.wedoogift.common.models.Distribution;
import fr.wedoogift.common.models.JsonEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static fr.wedoogift.common.utils.AppConstant.COMPANY_NOT_FOUND;
import static fr.wedoogift.common.utils.AppConstant.EXCEPTION_THROWED;

@Service
public abstract class CommonService<T extends JsonEntry> {
    @Autowired
    protected ObjectMapper objectMapper;

    Logger logger = LoggerFactory.getLogger(CommonService.class);


    protected void writeJsonOnOutputFile(File fileOutput, JsonEntry entries) throws IOException {
        objectMapper.writeValue(fileOutput, entries);
    }

    protected T readJsonFromFile(File inputFile, T defaultVal) throws IOException {
        T enties = (T) objectMapper.readValue(inputFile,
                defaultVal.getClass());
        return enties==null? defaultVal: enties;

    }

    protected Company getCompanyById(Long companyId, JsonEntry<? extends CommonUser, ? extends Distribution> entries) throws NotFoundElementException {
        Optional<Company> companyById = entries.getCompanies().stream().filter(
                company -> company.getId().equals(companyId)
        ).findFirst();
        companyById.orElseThrow(() -> {
            NotFoundElementException notFoundElementException = new NotFoundElementException(COMPANY_NOT_FOUND + companyId);
            logger.error(EXCEPTION_THROWED, notFoundElementException);
            return notFoundElementException;
        });
        return companyById.get();
    }


    protected Long getLastDistributionId(JsonEntry<? extends CommonUser, ? extends Distribution> entries) {
        Distribution lastDistribution = entries.getDistributions().stream()
                .max((o1, o2) -> (int) (o1.getId() - o2.getId())).orElse(null);
        return lastDistribution != null ? lastDistribution.getId() : 0;
    }
}
