package fr.wedoogift.common.services;

import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.utils.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public interface IService<T> {
    Logger logger = LoggerFactory.getLogger(IService.class);

    public Result distruteToUser(Long userId, Long companyId, T balance)
            throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount;
}
