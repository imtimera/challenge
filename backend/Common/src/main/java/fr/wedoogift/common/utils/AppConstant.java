package fr.wedoogift.common.utils;

public class AppConstant {
    public static final String INPUT_FILE_LEVEL1 = "LevelOne/src/main/resources/data/input.json";
    public static final String OUTPUT_EXPECTED_FILE_LEVEL1 = "LevelOne/src/main/resources/data/output-expected.json";
    public static final String INPUT_FILE_LEVEL2 = "LevelTwo/src/main/resources/data/input.json";
    public static final String OUTPUT_EXPECTED_FILE_LEVEL2 = "LevelTwo/src/main/resources/data/output-expected.json";
    public static final String COMPANY_NOT_FOUND = "Company not found for id: ";
    public static final String USER_NOT_FOUND = "User not found for id: ";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String EXCEPTION_THROWED = "Exception throwed :";
    public static final String COMPANY_BALANCE_IS_INSUFFICIENT = "Company balance is insufficient";
    public static final String NEW_AMOUNT_ADDED = "A new amount has been added to the user's account";
    public static final String DEFAULT_GENERATE="Default expected result is generate";
    public static final String INVALID_AMOUNT="Invalid amout : ";

}
