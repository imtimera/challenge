package fr.wedoogift.common.utils;

import org.springframework.http.HttpStatus;

public class Result {
    private HttpStatus status;

    private String description;

    private String error;

    public Result(HttpStatus status, String description, String error) {
        this.status = status;
        this.description = description;
        this.error = error;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}