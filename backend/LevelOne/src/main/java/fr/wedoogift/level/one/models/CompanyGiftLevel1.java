package fr.wedoogift.level.one.models;

public class CompanyGiftLevel1 {
    private long company_id;
    private double amount;
    private long user_id;

    public CompanyGiftLevel1() {
    }

    public CompanyGiftLevel1(long company_id, double amount, long user_id) {
        this.company_id = company_id;
        this.amount = amount;
        this.user_id = user_id;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }
}
