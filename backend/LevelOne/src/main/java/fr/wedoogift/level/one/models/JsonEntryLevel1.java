package fr.wedoogift.level.one.models;

import fr.wedoogift.common.models.Distribution;
import fr.wedoogift.common.models.JsonEntry;

import java.util.List;

public class JsonEntryLevel1 extends JsonEntry<UserLevel1, Distribution> {
    public JsonEntryLevel1() {
        super();
    }

    public JsonEntryLevel1(List users, List companies, List distributions) {
        super(users, companies, distributions);
    }

}
