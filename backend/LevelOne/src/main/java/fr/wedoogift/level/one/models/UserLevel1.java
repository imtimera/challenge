package fr.wedoogift.level.one.models;

import fr.wedoogift.common.models.CommonUser;

public class UserLevel1 extends CommonUser<Double> {

    public UserLevel1() {
        super();
    }

    public UserLevel1(Long id, double amount) {
        this.id = id;
        this.balance = amount;
    }

    public void receiveDistribution(Double amount) {
        this.balance +=amount;
    }
}
