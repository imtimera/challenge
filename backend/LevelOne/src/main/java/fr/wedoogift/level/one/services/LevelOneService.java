package fr.wedoogift.level.one.services;

import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.models.CommonUser;
import fr.wedoogift.common.models.Company;
import fr.wedoogift.common.models.Distribution;
import fr.wedoogift.common.models.JsonEntry;
import fr.wedoogift.common.services.CommonService;
import fr.wedoogift.common.services.IService;
import fr.wedoogift.common.utils.Result;
import fr.wedoogift.level.one.models.JsonEntryLevel1;
import fr.wedoogift.level.one.models.UserLevel1;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static fr.wedoogift.common.utils.AppConstant.COMPANY_BALANCE_IS_INSUFFICIENT;
import static fr.wedoogift.common.utils.AppConstant.DATE_FORMAT;
import static fr.wedoogift.common.utils.AppConstant.EXCEPTION_THROWED;
import static fr.wedoogift.common.utils.AppConstant.INPUT_FILE_LEVEL1;
import static fr.wedoogift.common.utils.AppConstant.INVALID_AMOUNT;
import static fr.wedoogift.common.utils.AppConstant.NEW_AMOUNT_ADDED;
import static fr.wedoogift.common.utils.AppConstant.OUTPUT_EXPECTED_FILE_LEVEL1;
import static fr.wedoogift.common.utils.AppConstant.USER_NOT_FOUND;


@Service
public class LevelOneService extends CommonService<JsonEntryLevel1> implements IService<Double> {

    //use Transaction for database query (rollback if something wrong)
    //@Transactional
    @Override
    public Result distruteToUser(Long userId, Long companyId, Double balance)
            throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        if (balance <=0){
            throw new InvalidAmount(INVALID_AMOUNT + balance);
        }
        File inputFile = getFileFromResources(INPUT_FILE_LEVEL1);
        JsonEntryLevel1 inputEntries = readJsonFromFile(inputFile, new JsonEntryLevel1());
        JsonEntryLevel1 outputEntries;

        long lastId;
        Distribution distribution;
        File outputFile = getFileFromResources(OUTPUT_EXPECTED_FILE_LEVEL1);
        try {
            outputEntries = readJsonFromFile(outputFile, new JsonEntryLevel1());
            //if output file exists read values from it and update data
            UserLevel1 user = getUserById(userId, outputEntries);
            Company company = getCompanyById(companyId, outputEntries);
            lastId = getLastDistributionId(outputEntries) + 1;
            distribution = doDistribution(balance, lastId, company, user);
            for (UserLevel1 user1 : outputEntries.getUsers()) {
                if (user1.getId().equals(userId)) {
                    user1.setBalance(user.getBalance());
                }
            }
            for (Company company1 : outputEntries.getCompanies()) {
                if (company1.getId().equals(companyId)) {
                    company1.setBalance(company.getBalance());
                }
            }
        } catch (IOException exception) {
            //If file not exists catch exception & initialize output=input
            outputEntries = inputEntries;
            UserLevel1 user = getUserById(userId, outputEntries);
            Company company = getCompanyById(companyId, outputEntries);
            lastId = getLastDistributionId(inputEntries) + 1;
            distribution = doDistribution(balance, lastId, company, user);
        }
        outputEntries.addDistribution(distribution);

        writeJsonOnOutputFile(outputFile, outputEntries);
        return new Result(HttpStatus.OK,
                NEW_AMOUNT_ADDED, null);
    }

    private UserLevel1 getUserById(Long userId, JsonEntry<UserLevel1, Distribution> entries) throws NotFoundElementException {
        Optional<UserLevel1> userById = entries.getUsers().stream().filter(
                user -> user.getId().equals(userId)
        ).findFirst();

        userById.orElseThrow(() -> {
            NotFoundElementException notFoundElementException = new NotFoundElementException(USER_NOT_FOUND + userId);
            logger.error(EXCEPTION_THROWED, notFoundElementException);
            return notFoundElementException;
        });
        return userById.get();
    }

    public double getUserBalance(Long userId) throws IOException, NotFoundElementException {
        JsonEntry outputEntries;
        try {
            File outputFile = getFileFromResources(OUTPUT_EXPECTED_FILE_LEVEL1);
            outputEntries = readJsonFromFile(outputFile,
                    new JsonEntryLevel1());
        } catch (IOException exception) {
            File inputFile = getFileFromResources(INPUT_FILE_LEVEL1);
            outputEntries = readJsonFromFile(inputFile,
                    new JsonEntryLevel1());
        }
        return getUserById(userId, outputEntries).getBalance();
    }

    private File getFileFromResources(String filename) {
        return new File(filename);
    }

    /*protected void writeJsonOnOutputFile(JsonEntry entries) throws IOException {
        //final String absolutePath = new FileSystemResource("").getFile().getAbsolutePath();
        //absolutePath+"LevelOne"+OUTPUT_EXPECTED_FILE
        //new File(new FileSystemResource(OUTPUT_EXPECTED_FILE).getFile().getAbsolutePath()
        File file = new File(OUTPUT_EXPECTED_FILE_LEVEL1);

        this.objectMapper.writeValue(file, entries);
    }*/

    private Distribution doDistribution(double amount, Long lastId, Company company, CommonUser user) throws InsufficientBalanceException {
        if (company.getBalance() < amount) {
            final String message = COMPANY_BALANCE_IS_INSUFFICIENT +
                    ": " + company + "[Amout:" + company.getBalance() + "]";
            InsufficientBalanceException e = new InsufficientBalanceException(message);
            logger.error(message, e);
            throw e;
        }
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(365);

        company.giveDistribution(amount);
        user.receiveDistribution(amount);
        String startDateFormat = startDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
        String endDateFormat = endDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
        return new Distribution(lastId, amount, startDateFormat,
                endDateFormat, company.getId(), user.getId());
    }


}
