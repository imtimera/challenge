package fr.wedoogift.level.one.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.models.Company;
import fr.wedoogift.common.models.Distribution;
import fr.wedoogift.common.utils.Result;
import fr.wedoogift.level.one.models.JsonEntryLevel1;
import fr.wedoogift.level.one.models.UserLevel1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static fr.wedoogift.common.utils.AppConstant.INPUT_FILE_LEVEL1;
import static fr.wedoogift.common.utils.AppConstant.OUTPUT_EXPECTED_FILE_LEVEL1;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LevelOneServiceTest {

    @InjectMocks
    LevelOneService levelOneService;

    @Mock
    ObjectMapper objectMapper;

    Long user_id1 = 1L;
    Long company_id1 = 1L;
    double amount1 = 150;


    @Test(expected = IOException.class)
    public void distruteGiftToUser_InvalidInputFile_IOExceptionThrows() throws IOException,
            NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        doThrow(IOException.class).when(objectMapper).readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class);
        callservice(user_id1, company_id1, amount1);
    }

    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_InvalidUserId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final Long badUserId = 10L;

        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);
        distruteGiftToUser_InvalidUserId(badUserId);
    }

    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_NoOutputFile_InvalidUserId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final Long badUserId = 10L;

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);
        distruteGiftToUser_InvalidUserId(badUserId);
    }


    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_NoOutputFile_InvalidCompanyId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        Long badCompanyId = 10L;
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);

        distruteGiftToUser_InvalidCompanyId(badCompanyId, jsonEntry);
    }


    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_InvalidCompanyId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        Long badCompanyId = 10L;
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        distruteGiftToUser_InvalidCompanyId(badCompanyId, jsonEntry);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void distruteGiftToUser_InsufficientBalance_InsufficientBalanceExceptionThrows()
            throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        double amount = 10000L;
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        callservice(user_id1, company_id1, amount);
    }

    @Test()
    public void distruteGiftToUser_UserBalanceUpdated_ThenReturnOKResponse() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();
        final Company firstCompagny = jsonEntry.getCompanies().stream().findFirst().get();
        final double expected = firstCompagny.getBalance() - amount1;
        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        distruteGiftToUser_withOK(user_id1, firstCompagny.getId(), jsonEntry);
        assertEquals("new amount is subtracted to company's balance", expected, firstCompagny.getBalance());
    }

    @Test()
    public void distruteGiftToUser_CompagnyBalanceUpdated_ThenReturnOKResponse() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();
        final UserLevel1 firstUser = jsonEntry.getUsers().stream().findFirst().get();
        final double expected = firstUser.getBalance() + amount1;

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);

        distruteGiftToUser_withOK(firstUser.getId(), company_id1, jsonEntry);
        assertEquals("new amount is added to User's balance", expected, firstUser.getBalance());
    }

    private void distruteGiftToUser_withOK(Long userId, Long company_id, JsonEntryLevel1 jsonEntry)
            throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        callservice(userId, company_id, amount1);
    }



    //    public void distruteGiftToUser_InvalidUserId_NotFoundElementExceptionThrows()throws IOException, NotFoundElementException, InsufficientBalanceException {
    private JsonEntryLevel1 initJsonEntryLevel1() {
        UserLevel1 user1 = new UserLevel1(1L, 50.0);
        UserLevel1 user2 = new UserLevel1(2L, 100.0);
        List<UserLevel1> listUsers = Arrays.asList(user1, user2);
        Company company1 = new Company(1L, "wedoogifttest1", 2000);
        Company company2 = new Company(2L, "wedoogifttest2", 5000);
        List<Company> listCompanies = Arrays.asList(company1, company2);
        List<Distribution> listDistribution = new ArrayList<>();

        return new JsonEntryLevel1(listUsers, listCompanies, listDistribution);
    }

    public void distruteGiftToUser_InvalidUserId(Long badUserId) throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);
        callservice(badUserId, company_id1, amount1);

    }

    private Result callservice(Long userId, Long companyId, double amount1) throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        return levelOneService.distruteToUser(userId, companyId, amount1);
    }

    public void distruteGiftToUser_InvalidCompanyId(Long company_id, JsonEntryLevel1 jsonEntry) throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        callservice(user_id1, company_id, amount1);
    }

    @Test(expected = NotFoundElementException.class)
    public void getUserBalance_invalidUserId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException {
        Long badUserId = 11L;
        levelOneService.getUserBalance(badUserId);
    }

    @Test(expected = IOException.class)
    public void getUserBalance_NoInputFile_IOExceptionExceptionThrows() throws IOException, NotFoundElementException {

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);

        doThrow(IOException.class).when(objectMapper).readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class);
        levelOneService.getUserBalance(user_id1);
    }

    @Test()
    public void getUserBalance_WhenUserHasntnewDistrution_ReturnOldUserBalance() throws IOException, NotFoundElementException {
        JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();
        final UserLevel1 firstUser = jsonEntry.getUsers().stream().findFirst().get();
        final double expected = firstUser.getBalance();

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL1),
                JsonEntryLevel1.class)).thenReturn(jsonEntry);

        double actual = levelOneService.getUserBalance(user_id1);
        assertEquals("get User balance succes",expected, actual);
    }

    @Test()
    public void getUserBalance_WhenHasNewDistrubition_ReturnNewUserBalance() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();
        final UserLevel1 firstUser = jsonEntry.getUsers().stream().findFirst().get();
        final double expected = firstUser.getBalance()+amount1;

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL1),
                JsonEntryLevel1.class);

        distruteGiftToUser_withOK(user_id1, company_id1, jsonEntry);

        double actual = levelOneService.getUserBalance(user_id1);
        assertEquals("get User balance succes",expected, actual);
    }

    @Test(expected = InvalidAmount.class)
    public void distruteGiftToUser_negativeAmount_InvalidAmountThrows()
            throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {
        final double negatifAmount = -100;
        final JsonEntryLevel1 jsonEntry = initJsonEntryLevel1();
        final Company firstCompagny = jsonEntry.getCompanies().stream().findFirst().get();

        callservice(user_id1, firstCompagny.getId(), negatifAmount);
    }

    /*
    This Test can be use for generate expected-output
    For doing it, comment @Mock of objectMapper
    and instanciate a new ObjectMapper on commonService and comment @Autowired line
    */
    /*@Test
    public void distruteGiftoUser_ThenResultInOutputFileRealCall() throws IOException, NotFoundElementException, InsufficientBalanceException {
        Long userId1= new Long(1);
        Long companyId1= new Long(1);
        double amount1 = 50;
        callservice(userId1, companyId1, amount1);
        Long userId2= new Long(2);
        double amount2 = 100;
        callservice(userId2, companyId1, amount2);
        Long userId3= new Long(3);
        Long company_id2= new Long(2);
        double amount3 = 1000;
        callservice(userId3, company_id2, amount3);
    }*/


}