package fr.wedoogift.level.three;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={
		"fr.wedoogift.level.one", "fr.wedoogift.level.two",
		"fr.wedoogift.level.three", "fr.wedoogift.common"})
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

}
