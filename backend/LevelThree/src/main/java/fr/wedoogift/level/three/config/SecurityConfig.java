package fr.wedoogift.level.three.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

            httpSecurity
                        .csrf().disable()
                        .httpBasic()
                    .and()
                        .authorizeRequests()
                        .anyRequest().authenticated()
                    .and();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

        UserDetails userDetailsA = User.withUsername("user_a")
                                       .password("pwd_a")
                                       .passwordEncoder(passwordEncoder::encode)
                                       .roles("USER")
                                       .build();
        //THIS MUST BE A USER AVAILABLE IN THE DB
        UserDetails userDetailsB = User.withUsername("ibrahima")
                                        .password("pwd_i")
                                        .passwordEncoder(passwordEncoder::encode)
                                        .roles("USER")
                                        .build();
        UserDetails userDetailsAdmin = User.withUsername("wedoogift")
                                            .password("wedoogift")
                                            .passwordEncoder(passwordEncoder::encode)
                                            .roles("ADMIN","USER")
                                            .build();

        authenticationManagerBuilder.inMemoryAuthentication()
                                    .withUser(userDetailsA)
                                    .withUser(userDetailsB)
                                    .withUser(userDetailsAdmin);

    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }
}
