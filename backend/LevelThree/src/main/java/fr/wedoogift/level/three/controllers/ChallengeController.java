package fr.wedoogift.level.three.controllers;

import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.utils.Result;
import fr.wedoogift.level.one.models.CompanyGiftLevel1;
import fr.wedoogift.level.one.services.LevelOneService;
import fr.wedoogift.level.two.models.Balance;
import fr.wedoogift.level.two.models.CompanyGiftLevel2;
import fr.wedoogift.level.two.services.LevelTwoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@Controller
@RequestMapping(value = "/wedoogift/challenge/")
public class ChallengeController {
    private Logger logger = LoggerFactory.getLogger(ChallengeController.class);

    @Autowired
    private LevelOneService levelOneService;

    @Autowired
    private LevelTwoService levelTwoService;

    @PostMapping(value = "level1/distrute",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> levelOneDistruteGiftToUser(@Valid @NotNull @RequestBody CompanyGiftLevel1 gift) {
        String errorDescription ;
        try {
            final long user_id = gift.getUser_id();
            final long company_id = gift.getCompany_id();
            final double amount = gift.getAmount();
            return ok(levelOneService.distruteToUser(user_id,
                    company_id, amount));
        } catch (IOException exception) {
            errorDescription = exception.getMessage();
        } catch (NotFoundElementException e) {
            errorDescription = e.getMessage();
        } catch (InsufficientBalanceException e) {
            errorDescription = e.getMessage();
        } catch (InvalidAmount invalidAmount) {
            errorDescription = invalidAmount.getMessage();
        }
        logger.error(errorDescription);
        return status(INTERNAL_SERVER_ERROR).body(new Result(INTERNAL_SERVER_ERROR, "ERROR_CODE", errorDescription));

    }

    @GetMapping("level1/user/{user_id}/balance")
    public ResponseEntity<Double> levelOneGetUserBalance(@PathVariable long user_id) {
        String errorDescription;
        try {
            return ok(levelOneService.getUserBalance(user_id));
        } catch (IOException exception) {
            errorDescription = exception.getMessage();
        } catch (NotFoundElementException exception) {
            errorDescription = exception.getMessage();
        }
        logger.error(errorDescription);
        return status(INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping(value = "level2/distrute",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> levelTwoDistruteGiftToUser(@Valid @NotNull @RequestBody CompanyGiftLevel2 gift) {
        String errorDescription ;
        try {
            final long user_id = gift.getUser_id();
            final long company_id = gift.getCompany_id();
            final Balance balance = gift.getBalance();
            return ok(levelTwoService.distruteToUser(user_id,
                    company_id, balance));
        } catch (IOException exception) {
            errorDescription = exception.getMessage();
        } catch (NotFoundElementException e) {
            errorDescription = e.getMessage();
        } catch (InsufficientBalanceException e) {
            errorDescription = e.getMessage();
        } catch (InvalidAmount e) {
            errorDescription = e.getMessage();
        }
        logger.error(errorDescription);
        return status(INTERNAL_SERVER_ERROR).body(new Result(INTERNAL_SERVER_ERROR, "ERROR_CODE", errorDescription));
    }

    @PostMapping(value = "level2/generate",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> generateDefaultResult() {
        String errorDescription ;
        try {
            return ok(levelTwoService.generateExpectResult());
        } catch (IOException exception) {
            errorDescription = exception.getMessage();
        } catch (NotFoundElementException e) {
            errorDescription = e.getMessage();
        } catch (InsufficientBalanceException e) {
            errorDescription = e.getMessage();
        } catch (InvalidAmount e) {
            errorDescription = e.getMessage();
        }
        logger.error(errorDescription);

        return status(INTERNAL_SERVER_ERROR).body(new Result(INTERNAL_SERVER_ERROR, "ERROR_CODE", errorDescription));
    }

}
