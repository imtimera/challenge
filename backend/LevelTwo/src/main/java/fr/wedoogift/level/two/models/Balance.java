package fr.wedoogift.level.two.models;

import java.io.Serializable;

public class Balance implements Serializable {
    private Long wallet_id;
    private double amount;

    public Balance() {
    }

    public Balance(Long wallet_id, double amount) {
        this.wallet_id = wallet_id;
        this.amount = amount;
    }

    public Long getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(Long wallet_id) {
        this.wallet_id = wallet_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
