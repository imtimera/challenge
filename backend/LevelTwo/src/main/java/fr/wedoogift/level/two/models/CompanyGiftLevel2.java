package fr.wedoogift.level.two.models;

public class CompanyGiftLevel2 {
    private long company_id;
    private Balance balance;
    private long user_id;

    public CompanyGiftLevel2() {
    }

    public CompanyGiftLevel2(long company_id, Balance balance, long user_id) {
        this.company_id = company_id;
        this.balance = balance;
        this.user_id = user_id;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }
}
