package fr.wedoogift.level.two.models;

import fr.wedoogift.common.models.Distribution;

public class DistributionLevel2 extends Distribution {
    private Long wallet_id;

    public DistributionLevel2() {
    }

    public DistributionLevel2(Long id, double amount, String start_date, String end_date,
                              Long company_id, Long user_id, Long wallet_id) {
        super(id, amount, start_date, end_date, company_id, user_id);
        this.wallet_id = wallet_id;
    }

    public Long getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(Long wallet_id) {
        this.wallet_id = wallet_id;
    }
}
