package fr.wedoogift.level.two.models;

import fr.wedoogift.common.models.JsonEntry;

import java.util.ArrayList;
import java.util.List;

public class JsonEntryLevel2 extends JsonEntry<UserLevel2, DistributionLevel2> {
    List<Wallet> wallets;

    public JsonEntryLevel2() {
        super();
        this.wallets = new ArrayList<>();

    }

    public JsonEntryLevel2(List users, List list, List list2, List<Wallet> wallets) {
        super(users, list, list2);
        this.wallets = wallets;

    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }
}
