package fr.wedoogift.level.two.models;

import fr.wedoogift.common.models.JsonEntry;

import java.util.List;

public class JsonOutputLevel2 extends JsonEntry<UserLevel2, DistributionLevel2> {

    public JsonOutputLevel2() {
        super();

    }

    public JsonOutputLevel2(List users, List companies, List distributions) {
        super(users, companies, distributions);

    }
}
