package fr.wedoogift.level.two.models;

import fr.wedoogift.common.models.CommonUser;

import java.util.ArrayList;

public class UserLevel2 extends CommonUser<ArrayList<Balance>> {

    public UserLevel2() {
        super();
    }

    public UserLevel2(Long id, ArrayList<Balance> amount) {
        this.id = id;
        this.balance = amount;
    }


    @Override
    public void receiveDistribution(ArrayList<Balance> amounts) {
        for (Balance amount : amounts) {
            boolean isadded = false;
            for (Balance balance1 : this.balance) {
                if (!isadded && balance1.getWallet_id().equals(amount.getWallet_id())) {
                    isadded = true;
                    balance1.setAmount(balance1.getAmount() + amount.getAmount());
                }
            }
            if (!isadded) {
                this.balance.add(amount);
            }
        }

    }
}
