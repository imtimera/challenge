package fr.wedoogift.level.two.models;

import fr.wedoogift.level.two.enums.WalletType;

import java.io.Serializable;
import java.time.LocalDate;

public class Wallet implements Serializable {

    private Long id;

    private String name;

    private WalletType type;

    public Wallet() {
        super();
    }

    public Wallet(Long id, String name, WalletType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WalletType getType() {
        return type;
    }

    public void setType(WalletType type) {
        this.type = type;
    }

    public LocalDate calculateEndDate(LocalDate startDate){
        return null;
    }
}
