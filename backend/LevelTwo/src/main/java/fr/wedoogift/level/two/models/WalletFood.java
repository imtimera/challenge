package fr.wedoogift.level.two.models;

import fr.wedoogift.level.two.enums.WalletType;

import java.time.LocalDate;
import java.time.Month;

public class WalletFood extends Wallet{
    public WalletFood() {
        super(2L, WalletType.FOOD.name(), WalletType.FOOD);
    }

    @Override
    public LocalDate calculateEndDate(LocalDate startDate) {
        return LocalDate.of(startDate.getYear() + 1, Month.FEBRUARY, 28);
    }

}
