package fr.wedoogift.level.two.models;

import fr.wedoogift.level.two.enums.WalletType;

import java.time.LocalDate;
import java.time.Month;

public class WalletGift extends Wallet{

    public WalletGift() {
        super(1L, WalletType.GIFT.name(), WalletType.GIFT);
    }

    @Override
    public LocalDate calculateEndDate(LocalDate startDate) {
        return startDate.plusDays(365);
    }
}
