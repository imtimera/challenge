package fr.wedoogift.level.two.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.models.Company;
import fr.wedoogift.common.models.JsonEntry;
import fr.wedoogift.common.services.CommonService;
import fr.wedoogift.common.services.IService;
import fr.wedoogift.common.utils.Result;
import fr.wedoogift.level.two.models.Balance;
import fr.wedoogift.level.two.models.DistributionLevel2;
import fr.wedoogift.level.two.models.JsonEntryLevel2;
import fr.wedoogift.level.two.models.JsonOutputLevel2;
import fr.wedoogift.level.two.models.UserLevel2;
import fr.wedoogift.level.two.models.Wallet;
import fr.wedoogift.level.two.models.WalletFood;
import fr.wedoogift.level.two.models.WalletGift;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

import static fr.wedoogift.common.utils.AppConstant.COMPANY_BALANCE_IS_INSUFFICIENT;
import static fr.wedoogift.common.utils.AppConstant.DATE_FORMAT;
import static fr.wedoogift.common.utils.AppConstant.DEFAULT_GENERATE;
import static fr.wedoogift.common.utils.AppConstant.EXCEPTION_THROWED;
import static fr.wedoogift.common.utils.AppConstant.INPUT_FILE_LEVEL2;
import static fr.wedoogift.common.utils.AppConstant.INVALID_AMOUNT;
import static fr.wedoogift.common.utils.AppConstant.NEW_AMOUNT_ADDED;
import static fr.wedoogift.common.utils.AppConstant.OUTPUT_EXPECTED_FILE_LEVEL2;
import static fr.wedoogift.common.utils.AppConstant.USER_NOT_FOUND;

@Service
public class LevelTwoService extends CommonService implements IService<Balance> {

    @Autowired
    protected ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Result distruteToUser(Long userId, Long companyId, Balance balance)
            throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final double amount = balance.getAmount();
        if (amount <=0){
            throw new InvalidAmount(INVALID_AMOUNT + amount);
        }
        File inputFile = getFileFromResources(INPUT_FILE_LEVEL2);
        JsonEntryLevel2 inputEntries = (JsonEntryLevel2) readJsonFromFile(inputFile,
                new JsonEntryLevel2());
        JsonEntryLevel2 outputEntries;

        long lastId;
        DistributionLevel2 distribution;
        File outputFile = getFileFromResources(OUTPUT_EXPECTED_FILE_LEVEL2);
        try {
            outputEntries = (JsonEntryLevel2) readJsonFromFile(outputFile, new JsonEntryLevel2());
            //if output file exists read values from it and update data
            UserLevel2 user = getUserById(userId, outputEntries);
            Company company = getCompanyById(companyId, outputEntries);
            lastId = getLastDistributionId(outputEntries) + 1;
            distribution = doDistribution(balance, lastId, company, user);
            for (UserLevel2 user1 : outputEntries.getUsers()) {
                if (user1.getId().equals(userId)) {
                    user1.setBalance(user.getBalance());
                }
            }
            for (Company company1 : outputEntries.getCompanies()) {
                if (company1.getId().equals(companyId)) {
                    company1.setBalance(company.getBalance());
                }
            }
        } catch (IOException exception) {
            //If file not exists catch exception & initialize output=input
            outputEntries = inputEntries;
            UserLevel2 user = getUserById(userId, outputEntries);
            Company company = getCompanyById(companyId, outputEntries);
            lastId = getLastDistributionId(inputEntries) + 1;
            distribution = doDistribution(balance, lastId, company, user);
        }

        outputEntries.addDistribution(distribution);
        JsonEntry output = level2OutputWithoutWallet(outputEntries);
        writeJsonOnOutputFile(outputFile, output);
        return new Result(HttpStatus.OK,
                NEW_AMOUNT_ADDED, null);
    }

    private JsonOutputLevel2 level2OutputWithoutWallet(JsonEntryLevel2 outputEntries) {
        return new JsonOutputLevel2(outputEntries.getUsers(),
                outputEntries.getCompanies(), outputEntries.getDistributions());
    }

    private UserLevel2 getUserById(Long userId, JsonEntry<UserLevel2, DistributionLevel2> entries) throws NotFoundElementException {
        Optional<UserLevel2> userById = entries.getUsers().stream().filter(
                user -> user.getId().equals(userId)
        ).findFirst();

        userById.orElseThrow(() -> {
            NotFoundElementException notFoundElementException = new NotFoundElementException(USER_NOT_FOUND + userId);
            logger.error(EXCEPTION_THROWED, notFoundElementException);
            return notFoundElementException;
        });
        return userById.get();
    }


    protected DistributionLevel2 doDistribution(Balance balance, Long lastId, Company company, UserLevel2 user)
            throws InsufficientBalanceException {
        if (company.getBalance() < balance.getAmount()) {
            final String message = COMPANY_BALANCE_IS_INSUFFICIENT +
                    ": " + company + "[Amout:" + balance.getAmount() + "]";
            InsufficientBalanceException e = new InsufficientBalanceException(message);
            logger.error(message, e);
            throw e;
        }
        LocalDate startDate = LocalDate.now();
        Wallet wallet = balance.getWallet_id() == 1 ? new WalletGift() : new WalletFood();
        LocalDate endDate = wallet.calculateEndDate(startDate);

        company.giveDistribution(balance.getAmount());
        ArrayList<Balance> balances = new ArrayList<>();
        balances.add(balance);
        user.receiveDistribution(balances);
        String startDateFormat = startDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
        String endDateFormat = endDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
        return new DistributionLevel2(lastId, balance.getAmount(), startDateFormat,
                endDateFormat, company.getId(), user.getId(), balance.getWallet_id());
    }

    public Result generateExpectResult() throws NotFoundElementException,
            IOException, InsufficientBalanceException, InvalidAmount {
        final Long userId1 = 1L;
        final Long userId2 = 2L;
        final Long userId3 = 3L;
        final Long companyId1 = 1L;
        final Long companyId2 = 2L;
        final Long walletId1 = 1L;
        final Long walletId2 = 2L;

        final int amount1 = 50;
        final int amount2 = 100;
        final int amount3 = 1000;
        final int amount4 = 250;

        Balance balance = new Balance(walletId1, amount1);
        distruteToUser(userId1, companyId1, balance);
        balance = new Balance(walletId1, amount2);
        distruteToUser(userId2, companyId1, balance);
        balance = new Balance(walletId1, amount3);
        distruteToUser(userId3, companyId2, balance);
        balance = new Balance(walletId2, amount4);
        distruteToUser(userId1, companyId1, balance);
        return new Result(HttpStatus.OK,
                DEFAULT_GENERATE, null);
    }

    private File getFileFromResources(String filename) {
        return new File(filename);
    }
}
