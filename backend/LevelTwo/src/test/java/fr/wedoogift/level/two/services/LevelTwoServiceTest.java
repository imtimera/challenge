package fr.wedoogift.level.two.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.wedoogift.common.exceptions.InsufficientBalanceException;
import fr.wedoogift.common.exceptions.InvalidAmount;
import fr.wedoogift.common.exceptions.NotFoundElementException;
import fr.wedoogift.common.models.Company;
import fr.wedoogift.common.models.Distribution;
import fr.wedoogift.common.utils.Result;
import fr.wedoogift.level.two.enums.WalletType;
import fr.wedoogift.level.two.models.Balance;
import fr.wedoogift.level.two.models.JsonEntryLevel2;
import fr.wedoogift.level.two.models.UserLevel2;
import fr.wedoogift.level.two.models.Wallet;
import fr.wedoogift.level.two.models.WalletFood;
import fr.wedoogift.level.two.models.WalletGift;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static fr.wedoogift.common.utils.AppConstant.INPUT_FILE_LEVEL2;
import static fr.wedoogift.common.utils.AppConstant.OUTPUT_EXPECTED_FILE_LEVEL2;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LevelTwoServiceTest {
    @InjectMocks
    LevelTwoService levelTwoService;

    @Mock
    ObjectMapper objectMapper;

    Long userId1 = 1L;
    Long companyId1 = 1L;
    Long walletId1 = 1L;
    Long walletId2 = 2L;
    Balance balance = new Balance(walletId1, 150);


    @Test(expected = IOException.class)
    public void distruteToUserLevel2_InvalidInputFile_IOExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        doThrow(IOException.class).when(objectMapper).readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class);
        callServiceLevel2(userId1, companyId1, balance);
    }

    @Test(expected = NotFoundElementException.class)
    public void distruteToUserLevel2_InvalidUserId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final Long badUserId = 10L;

        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);
        distruteGiftToUser_InvalidUserId(badUserId);
    }

    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_NoOutputFile_InvalidUserId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final Long badUserId = 10L;

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class);
        distruteGiftToUser_InvalidUserId(badUserId);
    }

    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_NoOutputFile_InvalidCompanyId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        Long badCompanyId = 10L;
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();

        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class);

        distruteGiftToUser_InvalidCompanyId(badCompanyId, jsonEntry);
    }


    @Test(expected = NotFoundElementException.class)
    public void distruteGiftToUser_InvalidCompanyId_NotFoundElementExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        Long badCompanyId = 10L;
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        distruteGiftToUser_InvalidCompanyId(badCompanyId, jsonEntry);
    }

    @Test()
    public void distruteGiftToUser_UserBalanceUpdated_ThenReturnOKResponse() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();
        final Company firstCompagny = jsonEntry.getCompanies().stream().findFirst().get();
        final double expected = firstCompagny.getBalance() - balance.getAmount();
        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        distruteGiftToUserLevel2_withOK(userId1, firstCompagny.getId(), jsonEntry);
        assertEquals("new amount is subtracted to company's balance", expected, firstCompagny.getBalance());
    }

    @Test()
    public void distruteGiftToUser_CompagnyBalanceUpdated_ThenReturnOKResponse() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();
        final UserLevel2 firstUser = jsonEntry.getUsers().stream().findFirst().get();
        final double expected = firstUser.getBalance().stream().mapToDouble(el -> el.getAmount()).sum()
                + balance.getAmount();


        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class);

        distruteGiftToUserLevel2_withOK(firstUser.getId(), companyId1, jsonEntry);
        final Double actual = firstUser.getBalance().stream().mapToDouble(el -> el.getAmount()).sum();
        assertEquals("new amount is added to User's balance", expected, actual);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void distruteGiftToUser_InsufficientBalance_InsufficientBalanceExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        double amount = 10000L;
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();

        when(objectMapper.readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        callServiceLevel2(userId1, companyId1, new Balance(3L, amount));
    }

    @Test(expected = InvalidAmount.class)
    public void distruteGiftToUser_NegativeOrZeroAmount_InvalidAmountBalanceExceptionThrows() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        double amount = -10.99;

        callServiceLevel2(userId1, companyId1, new Balance(3L, amount));
    }

    @Test
    public void generateExpectResult_ThenResultInOutputFile() throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();
        doThrow(IOException.class).when(objectMapper).readValue(new File(OUTPUT_EXPECTED_FILE_LEVEL2),
                JsonEntryLevel2.class);

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry, jsonEntry, jsonEntry,jsonEntry, jsonEntry);

        levelTwoService.generateExpectResult();
    }


    /*
    This Test can be use for generate expected-output
    For doing it, comment @Mock of objectMapper
    and instanciate a new ObjectMapper on commonService
    */
    /*@Test()
    public void generateExpectResult_ThenResultInOutputFileRealCall()
            throws IOException, NotFoundElementException, InsufficientBalanceException {
        levelTwoService.generateExpectResult();
    }*/

    private void distruteGiftToUserLevel2_withOK(Long userId, Long company_id, JsonEntryLevel2 jsonEntry) throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        callServiceLevel2(userId, company_id, balance);
    }

    public void distruteGiftToUser_InvalidCompanyId(Long company_id, JsonEntryLevel2 jsonEntry) throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);

        callServiceLevel2(userId1, company_id, balance);
    }

    public void distruteGiftToUser_InvalidUserId(Long badUserId) throws IOException, InsufficientBalanceException, NotFoundElementException, InvalidAmount {
        final JsonEntryLevel2 jsonEntry = initJsonEntryLevel2();

        when(objectMapper.readValue(new File(INPUT_FILE_LEVEL2),
                JsonEntryLevel2.class)).thenReturn(jsonEntry);
        callServiceLevel2(badUserId, companyId1, balance);

    }

    private JsonEntryLevel2 initJsonEntryLevel2() {
        final ArrayList<Balance> balances1 = new ArrayList<>();
        balances1.add(balance);

        final ArrayList<Balance> balances2 = new ArrayList<>();
        balances2.addAll(balances1);
        balances2.add(new Balance(walletId2, 100));

        final ArrayList<Wallet> wallets = new ArrayList<>();
        wallets.add(new WalletFood());
        wallets.add(new WalletGift());

        UserLevel2 user1 = new UserLevel2(1L, balances1);
        UserLevel2 user2 = new UserLevel2(2L, balances2);
        UserLevel2 user3 = new UserLevel2(3L, balances2);
        final List<UserLevel2> listUsers = Arrays.asList(user1, user2, user3);

        Company company1 = new Company(1L, "wedoogifttest1", 2000);
        Company company2 = new Company(2L, "wedoogifttest2", 5000);
        final List<Company> listCompanies = Arrays.asList(company1, company2);
        final List<Distribution> listDistribution = new ArrayList<>();

        return new JsonEntryLevel2(listUsers, listCompanies, listDistribution, wallets);

    }


    private Result callServiceLevel2(Long badUserId, Long company_id1, Balance balance) throws IOException, NotFoundElementException, InsufficientBalanceException, InvalidAmount {
        return levelTwoService.distruteToUser(badUserId, company_id1, balance);
    }

}